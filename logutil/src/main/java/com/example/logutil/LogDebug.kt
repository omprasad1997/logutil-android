package com.example.logutil

import android.util.Log

class LogDebug {

    private val TAG = "Super_awesome_app"

    private fun d(message:String){
        Log.d(TAG, "d: $message")
    }
}